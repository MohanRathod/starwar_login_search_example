import React from 'react';
import { Link } from 'react-router-dom';

import { userService } from '../_services';

class HomePage extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            user: {},
            users: [],
            searchString: ''
        };
    }

    componentDidMount() {
        this.setState({ 
            user: JSON.parse(localStorage.getItem('user')),
            users: { loading: true }
        });
        userService.getAll().then(users => this.setState({ users }));
    }

    updateResults = (e) => {
        const { value } = e.target;
        this.setState({
          searchString: value,
          users: [],
        });
        userService.find(value).then(users => this.setState({ users }));
    }

    render() {
        const { user, users } = this.state;
        return (
            <div className="col-md-12 col-md-offset-3">
                <div className="col-md-6 col-md-offset-0">
                    <h1>Hi {user.name}!</h1>
                </div>
                <div className="col-md-6 col-md-offset-0">
                    <p><Link to="/login">Logout</Link></p>
                </div>
                <div className="col-md-12 col-md-offset-0">
                    Search:
                    <input
                        className='search-textfield'
                        type='text'
                        placeholder='Search For People'
                        value={this.state.searchString}
                        onChange={this.updateResults}
                        />
                    {users.loading && <em>Loading users...</em>}
                    <div>
                        {users.length &&
                            <ul>
                                {users.map((user, index) =>
                                    <li key={user.birth_year}>
                                        {'Name: ' + user.name + ' Birth-Year:  ' + user.birth_year}
                                    </li>
                                )}
                            </ul>
                        }
                    </div>
                </div>
                
            </div>
        );
    }
}

export { HomePage };