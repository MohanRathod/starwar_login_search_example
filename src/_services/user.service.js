import config from 'config';
import { authHeader } from '../_helpers';

export const userService = {
    login,
    logout,
    getAll,
    find
};

function login(username, password) {

    return fetch(`${config.swUrl}/people/1/`)
        .then(response => response.json())
        .then(user => {
            console.log("USER : " + JSON.stringify(user));
            // login successful if there's a user in the response

            // store user details and basic auth credentials in local storage 
            // to keep user logged in between page refreshes

            if(user.name === username && user.birth_year === password){
                user.authdata = window.btoa(username + ':' + password);
                localStorage.setItem('user', JSON.stringify(user));

                return user;
            } else {
                console.log('Username or password is incorrect !!!');
                throw("Username or password is incorrect");
            }

            
        });
}

function logout() {
    // remove user from local storage to log user out
    localStorage.removeItem('user');
}

function getAll() {

    return fetch(`${config.swUrl}/people/`)
    .then(response => response.json())
    .then(users => {

        return users.results;
    });
}

function find(string) {

    return fetch(`${config.swUrl}/people/?search=${string}`)
    .then(response => response.json())
    .then(users => {

        return users.results;
    });
}